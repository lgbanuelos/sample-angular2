export class Query {
  name: string;
  startDate: Date;
  endDate: Date;
}

export class Relation {
    href: string;
}

export class PlantInventoryEntry {
    name: string;
    description: string;
    price: number;
    _links: {[rel:string]: Relation};
}

export class BusinessPeriod {
    startDate: Date;
    endDate: Date;
}

export class PurchaseOrder {
    plant: PlantInventoryEntry;
    rentalPeriod: BusinessPeriod;
    status: string;
    total: number;   
}