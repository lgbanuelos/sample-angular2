import { Component, Output, EventEmitter } from '@angular/core';
import { Query } from "app/sales/definitions";
import { PlantCatalogService } from "app/sales/requisition/plant-catalog.service";

@Component({
  selector: 'query-form',
  templateUrl: './query-form.component.html'
})
export class QueryFormComponent {
    @Output() executeQueryEvent: EventEmitter<Query> = new EventEmitter();
    query: Query = {name: '', startDate: new Date(), endDate: new Date()};
}
