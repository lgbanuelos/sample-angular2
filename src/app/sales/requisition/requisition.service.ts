import { Injectable } from "@angular/core";
import { PlantInventoryEntry, Query, PurchaseOrder } from "app/sales/definitions";
import {Http, URLSearchParams} from "@angular/http";

import * as moment from "moment";
import { Router } from "@angular/router";

@Injectable()
export class RequisitionService {
    order: PurchaseOrder = new PurchaseOrder();
    constructor(private http: Http, private router: Router){}

    setNewOrderInformation(plant: PlantInventoryEntry, query: Query) {
        this.order = new PurchaseOrder();
        this.order.plant = plant;
        this.order.rentalPeriod = {startDate: query.startDate, endDate: query.endDate};
        this.order.total = (moment(query.endDate).diff(moment(query.startDate), 'days') + 1) * plant.price; 
    }
    createPurchaseOrder() {
        this.http
            .post('http://localhost:3000/api/sales/orders', this.order)
            .subscribe(resp => console.log(resp));
        this.router.navigateByUrl('');
    }
}