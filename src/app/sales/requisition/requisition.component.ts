import { Component } from '@angular/core';
import { Query, PlantInventoryEntry } from "app/sales/definitions";
import { PlantCatalogService } from "app/sales/requisition/plant-catalog.service";
import { RequisitionService } from "app/sales/requisition/requisition.service";

@Component({
  selector: 'requisition',
  templateUrl: './requisition.component.html',
  styles: [`
        .nav-tabs > li > a {
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        background-color: #ddd;
        border-color: #ffffff;
        color:#888;
        text-align:left;
        font-size:large;
        padding-top: 18px;
        padding-bottom: 18px;
        padding-left: 30px;
        }
        .nav-tabs > li.active > a,
        .nav-tabs > li.active > a:hover,
        .nav-tabs > li.active > a:focus {
            color: #fff;
            background-color: #000066;
        }
  `]})
export class RequisitionComponent {
    query: Query;
    plant: PlantInventoryEntry;
    isQueryTabActive = true;
    isResultTabActive = false;
    isSummaryTabActive = false;

    constructor(private catalog: PlantCatalogService, private requisitionService: RequisitionService) {}

    executeQuery(query:Query) {
        this.query = query;
        console.log(JSON.stringify(query));
        this.catalog.executeQuery(query);
        this.isQueryTabActive = false;
        this.isResultTabActive = true;
    }

    selectPlant(plant:PlantInventoryEntry) {
        this.plant = plant;
        this.requisitionService.setNewOrderInformation(plant, this.query);
        this.isResultTabActive = false;
        this.isSummaryTabActive = true;
    }

}
