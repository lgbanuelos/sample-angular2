import { Component, Input } from "@angular/core";
import { RequisitionService } from "app/sales/requisition/requisition.service";
import { PurchaseOrder } from "app/sales/definitions";

@Component({
  selector: 'order-summary',
  templateUrl: './order-summary.component.html'
})
export class OrderSummaryComponent {
  @Input() order: PurchaseOrder;
  constructor(private requisitionService: RequisitionService) {}
}