import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PlantCatalogService } from "app/sales/requisition/plant-catalog.service";
import { QueryResultComponent } from "app/sales/requisition/query-result.component";
import { QueryFormComponent } from "app/sales/requisition/query-form.component";
import { RequisitionComponent } from "app/sales/requisition/requisition.component";
import { RequisitionService } from "app/sales/requisition/requisition.service";
import { OrderSummaryComponent } from "app/sales/requisition/order-summary.component";
import { OrderDashboardComponent } from "app/sales/management/order-dashboard.component";

import { RouterModule }   from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    QueryFormComponent,
    QueryResultComponent,
    OrderSummaryComponent,
    OrderDashboardComponent,
    RequisitionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/management', pathMatch: 'full' },
      { path: 'requisition', component: RequisitionComponent },
      { path: 'management', component: OrderDashboardComponent }
    ])
  ],
  providers: [PlantCatalogService, RequisitionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
