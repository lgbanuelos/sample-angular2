import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <a routerLink="/requisition">Requisition</a>
    <a routerLink="/management">Orders dashboard</a>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
}
