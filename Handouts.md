
In this year, I changed a little bit the approach because I started writing the front-end application for RentIt's information system, rather than BuildIt's. In fact, the change makes a lot of sense because, I distributed the Blueprint API for RentIt's system. In last year's code, I was assuming that the interactions happened in BuildIt side, but the interactions were directly forwarded to RentIt. Thus, it was possible to create the PO when the PHR was created, all of these from the front-end. The correct approach would then be that the front-end gathers the information for creating the PHR and sends the partial PHR to the backend. Then, the backend creates the partial PO and sends the request to RentIt. Anyway, that would be the correct implementation. That is why, this year I decided to work in RentIt's side.

Let me now summarize what I did last week. I used the Angular CLI to create the skeleton application. Note that this skeleton, includes already one Angular component (i.e. `AppComponent` defined within `app.component.ts`). After introducing the basic concepts (e.g. templates, bidirectional data binding, components attributes/methods), I would say that `AppComponent` implements a form thatthe user would use to enter the information to query the plant catalog. __The first step in your session would be to refactor this code into a new component, say `QueryFormComponent`__.

The plan for the first part would be to complete all the components/services for `sales/requisition`.

* sales
** requisition
*** QueryFormComponent
*** QueryResultComponent
*** SummaryComponent
*** PlantCatalogService
*** RequisitionComponent (This componet will become the root component for requisition)
** order-mgmt
*** DashboardComponent (Listing POs. Grouped by status? Ordered by start date?)

# Commit 1

I refactored `QueryFormComponent`. Unfortunately, my `api-mock` was broken and I didn't install the alternative package (`drakov`?). To cope with this limitation, I modified the `PlantCatalogService` to serve a fixture and avoid the HTTP interaction.

# Commit 2, 3

Added `RequisitionComponent`. This component will mediate the interaction of other components in `sales/requisition`.

# Commit 4

Using `@Output` to redirect the call from `QueryFormComponent` to `RequisitionComponent`. The idea is to decouple the child components and services and use a parent component as a mediator. This would enable reusability of the components.

```
@Component({
  selector: 'query-form',
  template: `
  <input type="text" [(ngModel)]="query.name">
  <input type="date" [(ngModel)]="query.startDate">
  <input type="date" [(ngModel)]="query.endDate">
  <button (click)="executeQueryEvent.emit(query)">Query plant catalog</button>
  {{query|json}}
  `
})
export class QueryFormComponent {
    @Output() executeQueryEvent: EventEmitter<Query> = new EventEmitter();
    query: Query = new Query();
}
```

Note that `QueryFormComponet` now includes an attribute `executeQueryEvent` which is triggered by a click on the button and that in turn `emit`s a new event with `query` as payload.

To capture the event mentioned above, the template defined in `RequisitionComponent` is changed as follows:

```
    <query-form (executeQueryEvent)='executeQuery($event)'></query-form>
```

That means that `executeQueryEvent` is an input event that is captured by `RequisitionComponent`. As a result, the event triggers the execution of `executeQuery($event)`, where `$event` is the payload associated with the event (in this case the information for querying the plant catalog).

```
export class RequisitionComponent {
    query: Query;
    executeQuery(query:Query) {
        this.query = query;
        console.log(JSON.stringify(query));
    }
}
```

In the first version, the method `executeQuery` only copies the content of `query` and prints out `query` to the console.

# Commit 5

Wired up `RequisitionComponent` and `PlantCatalogService`. In this version, the query information is forwarded to the the service to execute the query. Recall that `executeQuery` is triggered by `QueryFormComponent`'s button.

Also checkout `PlantCatalogService`, I have reactivated the HTTP call (I finally installed drakov).

# Commit 6

In a way, I repeated the process above: 
* Added `RequisitionService` (Gathers information to create a PO),
* Added a column with a button to select a plant,
* Connected the select plant button with a method in `RequisitionComponent` via an output event
* The above method stores the values (Query -- for rental dates/Reference to PlantInventoryEntry)

BTW I also added new definitions (_Link and Relation to store information about hyperlinks).

# Commit 7

Added a new component to present a preview of the PO.

# Commit 8

Replaced inlined templates by HTML files.

# Commit 9

In this commit, I show how to inject the value stored by `RequisitionService` to the `SummaryComponent` using an `@Input` directive. The approach is based on event propagation: any change on the value `order` stored by `RequisitionService` will be propagated from `RequisitionService` to `SummaryComponent` using an event. Note that `SummaryComponent` is completly unaware of `RequisitionService` (cf. component decoupling).

# Commit 10

Added a method to trigger the creation of a purchase order and connected the method with the corresponding button. I found out that Drakov is very strick, such that it matches the body of the POST request. This makes it not convenient for testing purposes (we need something more relaxed).

The work around is to install docker (now docker runs natively on windows and mac) and use the following command:

```
run -ti --rm -v $PWD/rentit.md:/usr/src/app/api.md -p 3000:3000 ajnasz/api-mock
```

OPTIONAL: I also added moment.js (npm install --save moment) to precompute the total of a purchase order in the frontend.

# Commit 11

In this commit, I added support to routes. The idea is that the application has two perspectives: order dashboard (listing orders according to their status) and order requisition. The changes are the following:

* Add the router module and configuration (file app.module.ts)

```
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/management', pathMatch: 'full' },
      { path: 'requisition', component: RequisitionComponent },
      { path: 'management', component: OrderDashboardComponent }
    ])
  ],
```

The addition is `RouterModule`, which you should import.

* There is also the changes on the template associated to `app.component.ts` (the root component)

```
  template: `
    <a routerLink="/requisition">Requisition</a>
    <a routerLink="/management">Orders dashboard</a>
    <router-outlet></router-outlet>
  `
```

* To illustrate programmatic redirection I change the application such that it goes to the order dashboard after executing the creation of the purchase order. The changes on the code include:

Injecting the Router service:

```
    constructor(private http: Http, private router: Router){}
```

Then calling the redirection (RequistionService):

```
        this.router.navigateByUrl('');
```

THAT IS ALL I WOULD COVER.


